include (ExternalProject)

if (NOT UNIX)
 list(APPEND CL_ARGS "-DBUILD_SHARED_LIBS:BOOL=OFF" "-DBUILD_STATIC_LIBS:BOOL=ON")
endif()

if (ANDROID)
list(APPEND CL_ARGS 
  "-DANDROID_ABI=${ANDROID_ABI}"
  "-DANDROID_PLATFORM=${ANDROID_PLATFORM}"
  "-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=${CMAKE_LIBRARY_OUTPUT_DIRECTORY}"
  "-DANDROID_NDK=${ANDROID_NDK}"
  "-DCMAKE_MAKE_PROGRAM=${CMAKE_MAKE_PROGRAM}"
)
endif()

list(APPEND CL_ARGS 
  "-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}"
  "-DCMAKE_CXX_FLAGS=${CMAKE_CXX_FLAGS}"
  "-DCMAKE_TOOLCHAIN_FILE=${CMAKE_TOOLCHAIN_FILE}"
  "-DCMAKE_FIND_ROOT_PATH=${CMAKE_BINARY_DIR}/Extra/"
  "-DCMAKE_PREFIX_PATH=${CMAKE_BINARY_DIR}/Extra"
  "-DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/Extra"
)

# look for system deps to use
if (UNIX AND NOT EMSCRIPTEN AND NOT ANDROID)

find_package(ZLIB)
if (ZLIB_FOUND)
  message("Using system zlib")
  include_directories(${ZLIB_INCLUDE_DIRS})
else()
  message("System zlib not found. building it now...")
  list(APPEND DEPENDENCIES buildzlib)
endif()

find_path(tinyxml2_INCLUDE_DIR NAMES tinyxml2.h)
find_library(tinyxml2_LIBRARIES NAMES tinyxml2)
if (tinyxml2_INCLUDE_DIR AND tinyxml2_LIBRARIES)
  message("Using system tinyxml2")
  include_directories(${tinyxml2_INCLUDE_DIR})
else()
  message("System tinyxml2 not found. building it now...")
  list(APPEND DEPENDENCIES buildtinyxml2)
endif()

find_path(glm_INCLUDE_DIR NAMES glm.hpp PATH_SUFFIXES include/glm include)
if (glm_INCLUDE_DIR)
  message("Using system glm")
  include_directories(${glm_INCLUDE_DIR})
else()
  message("System glm not found. building it now...")
  list(APPEND DEPENDENCIES buildglm)
endif()

find_package(LibXml2)
if (LIBXML2_FOUND)
  message("Using system libxml2")
  include_directories(${LIBXML2_INCLUDE_DIRS})
else()
  message("System libxml2 not found. building it now...")
  include_directories("${CMAKE_BINARY_DIR}/Extra/include/libxml2/")
  list(APPEND DEPENDENCIES buildlibxml2)
endif()

if (NOT ANDROID)
  INCLUDE(FindPkgConfig)
  PKG_SEARCH_MODULE(SDL2 sdl2)
endif()

if (SDL2_FOUND)
  message("Using system SDL2")
  include_directories(${SDL2_INCLUDE_DIRS})
else()
  message("System SDL2 not found. building it now...")
  include_directories("${CMAKE_BINARY_DIR}/Extra/include/SDL2/")
  list(APPEND DEPENDENCIES buildSDL2)
  list(APPEND DEPENDENCIES buildSDL2_image) 
endif()

else()
  list(APPEND DEPENDENCIES  buildglm buildzlib buildtinyxml2 buildlibxml2 buildSDL2 buildSDL2_image)
endif()

list(APPEND DEPENDENCIES buildtmx buildCrash2D)

# done looking for deps

if ("buildzlib" IN_LIST DEPENDENCIES)
  ExternalProject_Add("buildzlib"
    GIT_REPOSITORY https://github.com/madler/zlib.git
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/zlib"
    CMAKE_ARGS ${CL_ARGS} 
  )
endif()

if ("buildtinyxml2" IN_LIST DEPENDENCIES)
  ExternalProject_Add("buildtinyxml2"
    GIT_REPOSITORY https://github.com/leethomason/tinyxml2.git
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/tinyxml2"
    CMAKE_ARGS ${CL_ARGS} "-DBUILD_TESTS=OFF" "-DBUILD_TESTING=OFF" 
  )
endif()

if ("buildlibxml2" IN_LIST DEPENDENCIES)
  ExternalProject_Add("buildlibxml2"
    GIT_REPOSITORY https://gitlab.gnome.org/cheeseboy16/libxml2.git
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/libxml2"
    CMAKE_ARGS ${CL_ARGS} "-DENABLE_FTP=OFF" "-DENABLE_HTTP=OFF" "-DENABLE_ICONV=OFF" "-DENABLE_ICU=OFF" "-DENABLE_IPV6=OFF" "-DENABLE_LEGACY=OFF" "-DENABLE_LZMA=OFF" "-DENABLEbuildzlib=OFF" 
  )
endif()

if ("buildCrash2D" IN_LIST DEPENDENCIES)
  ExternalProject_Add("buildCrash2D"
    GIT_REPOSITORY https://gitlab.com/cheeseboy16/Crash2D.git
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/Crash2D"
    CMAKE_ARGS ${CL_ARGS} 
  )
endif()

set(TMX_DEPS)
if ("buildlibxml2" IN_LIST DEPENDENCIES)
  list(APPEND TMX_DEPS "buildlibxml2")
endif()

if ("buildzlib" IN_LIST DEPENDENCIES)
  list(APPEND TMX_DEPS "buildzlib")
endif()

if ("buildtmx" IN_LIST DEPENDENCIES)
  ExternalProject_Add("buildtmx"
    GIT_REPOSITORY https://github.com/baylej/tmx.git
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/libtmx"
    CMAKE_ARGS ${CL_ARGS}
    DEPENDS "${TMX_DEPS}"
  )
endif()

if ("buildglm" IN_LIST DEPENDENCIES)
  ExternalProject_Add("buildglm"
    GIT_REPOSITORY https://github.com/g-truc/glm.git
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/glm"
    CMAKE_ARGS ${CL_ARGS} "-DGLM_TEST_ENABLE=OFF" 
  )
endif()

if ("buildSDL2" IN_LIST DEPENDENCIES)
 if (ANDROID)
   ExternalProject_Add("buildSDL2"
     GIT_REPOSITORY https://github.com/spurious/SDL-mirror.git
     GIT_SHALLOW 1
     SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/SDL2/SDL2"
     BUILD_COMMAND ""
     CONFIGURE_COMMAND ""
     CMAKE_COMMAND ""
     INSTALL_COMMAND ""
    )
  else() 
    ExternalProject_Add("buildSDL2"
      GIT_REPOSITORY https://github.com/spurious/SDL-mirror.git
      GIT_SHALLOW 1
      SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/SDL2/SDL2"
      CMAKE_ARGS ${CL_ARGS} "-DSDL_THREADS=TRUE"
    )
  endif()   
endif()

if ("buildSDL2_image" IN_LIST DEPENDENCIES)

  if (EMSCRIPTEN) # bug in their cmake
    set(PNG_FLAGS "-DPNG_HARDWARE_OPTIMIZATIONS=FALSE" "-DHAIKU=TRUE")
  endif()
  
  ExternalProject_Add("buildlibpng"
    GIT_REPOSITORY git://git.code.sf.net/p/libpng/code
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/libpng"
    CMAKE_ARGS ${CL_ARGS} "-DPNG_SHARED=FALSE" "-DPNG_STATIC=TRUE" "-DPNG_TESTS=FALSE" ${PNG_FLAGS}
    DEPENDS buildzlib
  )
  
  ExternalProject_Add("buildSDL2_image"
    GIT_REPOSITORY https://github.com/SDL-mirror/SDL_image.git
    GIT_SHALLOW 1
    SOURCE_DIR "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image"
    BUILD_COMMAND ""
    CONFIGURE_COMMAND ""
    CMAKE_COMMAND ""
    INSTALL_COMMAND ""
    DEPENDS buildSDL2 buildlibpng
  )
  
  if (NOT ANDROID)
    # SDL_image doesnt have cmake so we build as our own lib
    set(SDL_IMAGE_SRCS
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_bmp.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_gif.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_jpg.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_lbm.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_pcx.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_png.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_pnm.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_svg.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_tga.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_tif.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_webp.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_WIC.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_xcf.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_xpm.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_xv.c"
      "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/IMG_xxx.c"
    )
    set_source_files_properties(${SDL_IMAGE_SRCS} PROPERTIES GENERATED TRUE)
    add_library(SDL2_image STATIC ${SDL_IMAGE_SRCS})
    target_compile_definitions(SDL2_image PRIVATE "-DLOAD_PNG")
    add_dependencies(SDL2_image buildSDL2_image buildlibpng)
    list(APPEND DEPENDENCIES SDL2_image)
  endif()
  
endif()
