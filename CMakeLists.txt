cmake_minimum_required(VERSION 3.6)

if(POLICY CMP0063)
  cmake_policy(SET CMP0063 NEW)
endif()

set(CL_ARGS)

project(Platformer)

if (ANDROID)
  set(EXECUTABLE_NAME "main")
else()
  set(EXECUTABLE_NAME "GAYM")
endif()

set(DEPENDENCIES)

if (EMSCRIPTEN)
  set(CMAKE_EXECUTABLE_SUFFIX ".html")
endif(EMSCRIPTEN)

#add includes
if (ANDROID)
  set(GLAD_DIR glad/gles2/)
else()
  set(GLAD_DIR glad/gles3/)
endif()

set(CMAKE_FIND_ROOT_PATH "${CMAKE_BINARY_DIR}/Extra/")

include_directories(
 .
 "${CMAKE_BINARY_DIR}/Extra/include" 
 "${CMAKE_SOURCE_DIR}/external/SDL2/SDL2/include"
 "${CMAKE_SOURCE_DIR}/external/SDL2/SDL_image/"
 "${GLAD_DIR}/include/" 
 "src/" 
 "game/"
)

link_directories(
  "${CMAKE_BINARY_DIR}"
  "${CMAKE_BINARY_DIR}/Extra/lib" 
  "${CMAKE_BINARY_DIR}/Extra/lib64"
)

if (NOT SKIP_SUPERBUILD) 
  include("SuperBuild.cmake")
endif()

#add sources
file(GLOB ENGINE_SOURCES "src/*.cpp" "src/*.c")
file(GLOB GAME_SOURCES "game/*.cpp")

set(GLAD_SOURCES "${GLAD_DIR}/src/glad.c")

if (ANDROID)
  set(CMAKE_CXX_STANDARD 11)
else()
   set(CMAKE_CXX_STANDARD 17)
endif()

if (EMSCRIPTEN)
  set(EMEN_FLAGS "-s ALLOW_MEMORY_GROWTH=1 -s WASM=1 -s USE_WEBGL2=1 -O3")
  set(EMEN_LINK_FLAGS "--preload-file ${CMAKE_SOURCE_DIR}/assets@/assets --no-heap-copy")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=c++17 ${EMEN_FLAGS}")
  set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${EMEN_FLAGS} ${EMEN_LINK_FLAGS}")  
endif()

if (ANDROID)
  add_library(SDL2 SHARED IMPORTED)
  add_library(SDL2_image SHARED IMPORTED)
  string(TOLOWER ${CMAKE_BUILD_TYPE} ANDROID_BUILD_DIR)
  set(DISTRIBUTION_DIR ${CMAKE_SOURCE_DIR}/android/distribution/android/SDL2/intermediates/ndkBuild)
  set_target_properties(SDL2 PROPERTIES IMPORTED_LOCATION ${DISTRIBUTION_DIR}/${ANDROID_BUILD_DIR}/obj/local/${ANDROID_ABI}/libSDL2.so)
  set_target_properties(SDL2_image PROPERTIES IMPORTED_LOCATION ${DISTRIBUTION_DIR}/${ANDROID_BUILD_DIR}/obj/local/${ANDROID_ABI}/libSDL2_image.so)
  add_library(${EXECUTABLE_NAME} SHARED ${GLAD_SOURCES} ${ENGINE_SOURCES} ${GAME_SOURCES} "${CMAKE_SOURCE_DIR}/external/SDL2/SDL2/src/main/android/SDL_android_main.c" "main.cpp")
else()
  add_executable(${EXECUTABLE_NAME} ${GLAD_SOURCES} ${ENGINE_SOURCES} ${GAME_SOURCES} "main.cpp")
endif()

if (NOT SKIP_SUPERBUILD) 
  add_dependencies(${EXECUTABLE_NAME} ${DEPENDENCIES})
endif()

target_compile_definitions(${EXECUTABLE_NAME} PRIVATE RES_W=1920 RES_H=1080)

if (WIN32)
  set(SDL_DEPS imm32 version winmm)
  set(SDL_POSFIX "-static")
endif()

if (MSVC)
  set(ZLIB_LIB zlibstatic)
else()
   set(ZLIB_LIB z)
endif()

if (MINGW)
  set(CMAKE_EXE_LINKER_FLAGS " -static")
  target_link_libraries(${EXECUTABLE_NAME} -static-libgcc -static-libstdc++)
  target_link_libraries(${EXECUTABLE_NAME} mingw32)
endif()

if (ANDROID)
  set(SDL_LIBS SDL2_image SDL2)
else()
  set(SDL_LIBS SDL2_image SDL2main SDL2${SDL_POSFIX} ${SDL_DEPS})
endif()

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
  set(TINYXML2 tinyxml2d)
else()
  set(TINYXML2 tinyxml2)
endif()

target_link_libraries(${EXECUTABLE_NAME} Crash2D tmx ${SDL_LIBS} ${TINYXML2} xml2 png ${ZLIB_LIB})

if (MINGW)
 target_link_libraries(${EXECUTABLE_NAME} mingw32)
endif()

if (WIN32)
  target_link_libraries(${EXECUTABLE_NAME} opengl32)
elseif(ANDROID)
  target_link_libraries(${EXECUTABLE_NAME} GLESv2 log)
elseif (UNIX)
  target_link_libraries(${EXECUTABLE_NAME} GL dl)
endif()

set(SKIP_SUPERBUILD ON CACHE BOOL "Set to skip superbuild")
