#include "Input.hpp"
#include "Logger.hpp"

std::map<int, bool> Input::_keyPressed;
std::map<int, bool> Input::_keyHeld;
std::map<int, bool> Input::_keyReleased;

Gamepad Input::_gamepad[4];

ControlScheme Input::_controls;

Gamepad::Gamepad()
    : isConnected(false),
      id(-1),
      deadzone(0.5f),
      _buttonPressed{false},
      _buttonHeld{false},
      _buttonReleased{false},
      _axisPressed{false},
      _axisHeld{false},
      _axisReleased{false},
      _axes(0),
      _buttons(0) {}

bool Gamepad::Left() { return ((_axes[0] < -deadzone) || (_axes[6] < -deadzone)); }

bool Gamepad::Right() { return ((_axes[0] > deadzone) || (_axes[6] > deadzone)); }

bool Gamepad::Up() { return ((_axes[1] < -deadzone) || (_axes[7] < -deadzone)); }

bool Gamepad::Down() { return ((_axes[1] > deadzone) || (_axes[7] > deadzone)); }

bool Gamepad::ButtonPressed(GamepadButton id) {
  if (!isConnected) return false;

  switch (id) {
    default:
      return false;

    case GamepadButton::A:
      return _buttonPressed[0];

    case GamepadButton::B:
      return _buttonPressed[1];

    case GamepadButton::X:
      return _buttonPressed[2];

    case GamepadButton::Y:
      return _buttonPressed[3];

    case GamepadButton::Left:
      return Left() && (_axisPressed[0] || _axisPressed[6]);

    case GamepadButton::Right:
      return Right() && (_axisPressed[0] || _axisPressed[6]);

    case GamepadButton::Up:
      return Up() && (_axisPressed[1] || _axisPressed[7]);

    case GamepadButton::Down:
      return Down() && (_axisPressed[1] || _axisPressed[7]);
  }
}

bool Gamepad::ButtonHeld(GamepadButton id) {
  if (!isConnected) return false;

  switch (id) {
    default:
      return false;

    case GamepadButton::A:
      return _buttonHeld[0];

    case GamepadButton::B:
      return _buttonHeld[1];

    case GamepadButton::X:
      return _buttonHeld[2];

    case GamepadButton::Y:
      return _buttonHeld[3];

    case GamepadButton::Left:
      return Left();

    case GamepadButton::Right:
      return Right();

    case GamepadButton::Up:
      return Up();

    case GamepadButton::Down:
      return Down();
  }
}

bool Gamepad::ButtonReleased(GamepadButton id) { return false; }

void Gamepad::UpdateAxes(const float* axes, unsigned axesCount) {
  if (axesCount != _axes.size()) {
    _axes.clear();
    _axes.resize(axesCount);
  }

  for (unsigned j = 0; j < axesCount; j++) {
    _axes[j] = axes[j];

    if (std::abs(axes[j]) > deadzone) {
      if (_axisHeld[j])
        _axisPressed[j] = false;
      else
        _axisPressed[j] = true;

      _axisHeld[j] = true;
      _axisReleased[j] = false;
    } else {
      if (_axisHeld[j]) _axisReleased[j] = true;

      _axisPressed[j] = false;
      _axisHeld[j] = false;
    }
  }
}

void Gamepad::UpdateButtons(const unsigned char* buttons, unsigned buttonCount) {
  /*if (buttonCount != _buttons.size())
	{
		_buttons.clear();
		_buttons.resize(buttonCount);
	}

	for(unsigned j = 0; j < buttonCount; j++)
	{
		_buttons[j] = buttons[j];

		if (buttons[j] == GLFW_PRESS)
		{
			if (_buttonHeld[j])
				_buttonPressed[j] = false;
			else
				_buttonPressed[j] = true;

			_buttonHeld[j] = true;
			_buttonReleased[j] = false;
		}

		if (buttons[j] == GLFW_RELEASE)
		{
			_buttonReleased[j] = true;
			_buttonPressed[j] = false;
			_buttonHeld[j] = false;
		}
	}*/
}

Input& Input::Get() {
  static Input instance;
  return instance;
}

void Input::KeyDown(int key) {
  _keyPressed[key] = true;
  _keyHeld[key] = true;
  _keyReleased[key] = false;
}

void Input::KeyUp(int key) {
  _keyReleased[key] = true;
  _keyPressed[key] = false;
  _keyHeld[key] = false;
}

/*void Input::JoystickCallback(int joy, int event)
{
	if (event == GLFW_CONNECTED)
	{
		for (unsigned i = 0; i < 4; i++)
		{
			if (!_gamepad[i].isConnected)
			{
				_gamepad[i].isConnected = true;

				int axesCount;
				auto axes = glfwGetJoystickAxes(i, &axesCount);
				_gamepad[i].UpdateAxes(axes, axesCount);


				int buttonCount;
				auto buttons = glfwGetJoystickButtons(i, &buttonCount);
				_gamepad[i].UpdateButtons(buttons, buttonCount);

				_gamepad[i].id = joy;

				std::cout << "Connected gamepad " << joy+1
				<< " (" << glfwGetJoystickName(joy)
				<< ") as Player " << i+1 << std::endl;

				break;
			}
		}
	}

	else if (event == GLFW_DISCONNECTED)
	{
		for (unsigned i = 0;  i < 4;  i++)
		{
			if (_gamepad[i].id == joy)
			{
				std::cerr << "ERROR::GAMEPAD: Player " << i+1 << "'s gamepad has disconnected" << std::endl;
				_gamepad[i].isConnected = false;
				_gamepad[i].id = -1;
				break;
			}
		}
	}
}*/

void Input::ResetButtons() {
  for (auto&& k : _keyPressed) {
    k.second = false;
  }

  /*for (unsigned i = 0; i < 4; i++)
	{
		if (_gamepad[i].isConnected)
		{
			int axesCount;
			auto axes = glfwGetJoystickAxes(i, &axesCount);
			_gamepad[i].UpdateAxes(axes, axesCount);


			int buttonCount;
			auto buttons = glfwGetJoystickButtons(i, &buttonCount);
			_gamepad[i].UpdateButtons(buttons, buttonCount);
		}
	}*/
}

bool Input::ButtonPressed(GameControl id) {
  return _keyPressed[_controls.GetButton(id).key] || _gamepad[0].ButtonPressed(_controls.GetButton(id).button);
}

bool Input::ButtonHeld(GameControl id) {
  return _keyHeld[_controls.GetButton(id).key] || _gamepad[0].ButtonHeld(_controls.GetButton(id).button);
}

bool Input::ButtonReleased(GameControl id) { return _keyReleased[_controls.GetButton(id).key]; }

void Input::SetControlScheme(ControlScheme& controls) { _controls = controls; }
