#ifndef INPUT_HPP
#define INPUT_HPP

#include "ControlScheme.hpp"

#include <SDL.h>

#include <map>

/*
  buttons

a: 0
b: 1
x: 2
y: 3
select: 6
start: 7
lb: 4
rb: 5
l stick: 9
r stick: 10
xbox: 8

   axes

O: Left Stick Left(-1)/Right(1)
1: Left Stick Up(-1)/Down(1)
2: Left Trigger Released(-1)/Held(1)
3: Right Stick Left(-1)/Right(1)
4: Right Stick Up(-1)/Down(1)
5: Right Trigger Released(-1)/Held(1)
6: Dpad Left(-1)/Right(1)
7: Dpad Up(-1)/Down(1)
*/
class Gamepad {
 public:
  Gamepad();
  bool isConnected;
  int id;
  float deadzone;

  bool ButtonPressed(GamepadButton id);
  bool ButtonHeld(GamepadButton id);
  bool ButtonReleased(GamepadButton id);

  void UpdateAxes(const float* axes, unsigned axesCount);
  void UpdateButtons(const unsigned char* buttons, unsigned buttonCount);

 protected:
  bool Left();
  bool Right();
  bool Up();
  bool Down();

  bool _buttonPressed[16];
  bool _buttonHeld[16];
  bool _buttonReleased[16];

  bool _axisPressed[8];
  bool _axisHeld[8];
  bool _axisReleased[8];

  std::vector<float> _axes;
  std::vector<bool> _buttons;
};

class Input {
 public:
  static Input& Get();
  
  static void KeyDown(int key);
  static void KeyUp(int key);

  static void SetControlScheme(ControlScheme& controls);

  static bool ButtonPressed(GameControl id);
  static bool ButtonHeld(GameControl id);
  static bool ButtonReleased(GameControl id);

  static void ResetButtons();

 protected:
  Input(void) {}
  Input(Input const&);           // prevent copies
  void operator=(Input const&);  // prevent assignments
  static ControlScheme _controls;

  static std::map<int, bool> _keyPressed;
  static std::map<int, bool> _keyHeld;
  static std::map<int, bool> _keyReleased;

  static Gamepad _gamepad[4];
};

#endif
